###<h3>Installation of environment</h3>
* 1) Install NodeJS v5+ and npm v3+ on your machine.
* 2) Clone project
    ```
    $ git clone https://gitlab.com/Smertos/NewLauncher
    ```
* 3) Install dependencies and build project:
  ```
    $ npm install
  ```
