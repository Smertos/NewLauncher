import _ from 'underscore'
import FS from 'fs'
import Path from 'path'

export default {

  Utils: void(0),
  Ready: false,
  MaxLogLevel: 3,
  LogFileName: 'Launcher.log',


  LogLevel: {

    SEVERE: {
      value: 0,
      toString: () => "SEVERE"
    },

    WARN: {
      value: 1,
      toString: () => "WARN"
    },

    INFO: {
      value: 2,
      toString: () => "INFO"
    },

    DEBUG: {
      value: 3,
      toString: () => "DEBUG"
    }

  },

  MessagesStack: [],


  Log: function (message, level) {

    if(typeof message === 'undefined')
      return;

    if(
      level != this.LogLevel.SEVERE
      && level != this.LogLevel.WARN
      && level != this.LogLevel.INFO
      && level != this.LogLevel.DEBUG
    )
    {
      var level = this.LogLevel.INFO;
    }


    if(message instanceof Error)
    {
      var level = this.LogLevel.SEVERE;
      var message = message.message;
    }

    if(typeof message == 'string' && level.value <= this.MaxLogLevel) {

      FS.writeFileSync(this.Utils.BasePath(this.LogFileName), '[' + level.toString() + '] ' + message + '\n', {flag: 'a'});
      console.log('[' + level.toString() + '] ' + message);

      if(this.Ready)
      {

        if(this.MessagesStack.length != 0)
        {
          consoleWebContents.send('logstack', this.MessagesStack);

          this.MessagesStack = [];
        }

        consoleWebContents.send('log', {
          type: level.toString().replace(/severe/, 'ERROR'),
          message: message
        });

      }
      else
      {

        this.MessagesStack.push({
          type: level.toString().replace(/severe/, 'ERROR'),
          message: message
        });

      }


    }

  },


  Init: function (Utils) {
    this.Utils = Utils;

    FS.writeFileSync(this.Utils.BasePath(this.LogFileName), '-'.repeat(10) + 'Log Started - ' + new Date() + '-'.repeat(10) + '\n', {flag: 'a'});

    return this;
  }



}
