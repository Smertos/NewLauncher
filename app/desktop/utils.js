import os from 'os'
import path from 'path'
import mkdirp from 'mkdirp'
import electron from 'electron'

export default {

  LauncherDir: 'Launcher',

  OS: function () {
    switch (os.platform()) {
      case 'win32':    return     'win';
      case 'linux':    return     'linux';
      case 'darwin':   return     'osx';
      default:         return     os.platform();
    }
  },

  Arch: function () {
    switch (os.arch()) {
      case 'ia32':  return '32';
      case 'x64':   return '64';
      case 'arm':   return 'ARM';
      default:      return os.arch();
    }
  },

  BaseFolder: function () {
    switch (this.OS()) {
      case 'win':            return      path.resolve(process.env['LOCALAPPDATA'], this.LauncherDir);
      case 'osx':            return      path.resolve(process.env['LOCALAPPDATA'], this.LauncherDir);
      case 'linux': default: return      path.resolve(process.env['HOME'], '.' + this.LauncherDir);
    }
  },

  BasePath: function (localPath) {
    return path.resolve(this.BaseFolder(), localPath);
  },

  Init : function() {

    mkdirp.sync(this.BaseFolder());
  }
};
