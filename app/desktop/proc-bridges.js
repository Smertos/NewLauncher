

export default function() {

  /*  Listeners  */
  ipcMain.on('install', (e, data) => PubSub.publish('modpacks.install', data));
  ipcMain.on('update', (e, data) => PubSub.publish('modpacks.update', data));
  ipcMain.on('launch', (e, data) => PubSub.publish('modpacks.launch', data));
  ipcMain.on('modpacks-data', (e, data) => PubSub.publish('modpacks.got-data', data));
  ipcMain.on('downloader', (e, data) => PubSub.publish(data));

  /*  Fires  */
  PubSub.subscribe('modpacks.need-update', (e, data) => launcherWebContents.send('need-update', data));
  PubSub.subscribe('modpacks.installed', (e, data) => launcherWebContents.send('installed', data));
  PubSub.subscribe('downloader', (e, data) => launcherWebContents.send('downloader', data));

};
