import json from 'json-file'
import fs from 'fs'
import AOrder from 'async-order'
import touch from 'touch'

export default {

  ConfigName: 'config.json',
  ConfigLoaded: false,
  Utils: void(0),
  Logger: void(0),

  DefaultConfig: {
    InstalledModpacks: {}
  },

  GetConfigPath: function () {
    return this.Utils.BasePath(this.ConfigName);
  },

  Get: function (key) {
    if(!this.Config)
      this.Load();

    return this.Config[key];
  },

  Set: function (key, value) {
    if(!this.Config)
      this.Load();

    this.Config[key] = value;

    this.Save();

    return this;
  },

  Load: function () {

    let confPath = this.GetConfigPath();

    try {
      fs.accessSync(confPath, fs.F_OK | fs.R_OK | fs.W_OK);
    } catch(e) {
      touch.sync(confPath, this.DefaultConfig);
      fs.writeFileSync(confPath, JSON.stringify(this.DefaultConfig));
    }

    this.Config = JSON.parse(fs.readFileSync(confPath));
    this.Config = Object.assign(this.DefaultConfig, this.Config);

    this.Logger.Log('Config: ' + JSON.stringify(this.Config), this.Logger.LogLevel.DEBUG);
  },

  Save: function () {
    fs.writeFileSync(this.GetConfigPath(), JSON.stringify(this.Config));
  },

  Init: function(Utils, Logger) {
    this.Utils = Utils;
    this.Logger = Logger;

    PubSub.subscribe('config.save', () => {
      this.Save();
    });

    this.Load();

    return this;
  }
}
