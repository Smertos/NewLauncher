import electron from 'electron'
import path from 'path'
import ProcBridges from './proc-bridges.js'

const {app, BrowserWindow} = electron;

export default function(cb) {

  global.ipcMain = electron.ipcMain;

  app.on('window-all-closed', function onAllWindowsClosed() {
    if(process.platform != 'darwin')
      app.quit();
  });

  app.on('ready', function () {

    Promise.all([
      new Promise(res => PubSub.subscribe('console-did-finish-load', res)),
      new Promise(res => PubSub.subscribe('launcher-did-finish-load', res)),
      new Promise(res => PubSub.subscribe('everything-was-shown', res))
    ]).then(() => cb(app, launcherWin, consoleWin));

    let consoleWin = new BrowserWindow({
      width: 800, height: 480,
      x: 100, y: 40,
      minWidth: 400, minHeight: 100,
      resizeable: true,
      title: "Launcher",
      show: false,
      frame: false,
      enableLargerThanScreen: false,
      hasShadow: true,
      backgroundColor: "#fff"
    });

    consoleWin.on('close', () => {
      consoleWin.hide();
    });

    consoleWin.loadURL(
      'file:///' + path.resolve(__dirname, '../ui/console.html')
    );

    consoleWin.show();

    global.consoleWebContents = consoleWin.webContents;
    consoleWin.webContents.on('did-finish-load', () => PubSub.publish('console-did-finish-load'));


    let launcherWin = new BrowserWindow({
      width: 800, height: 480,
      center: true,
      minWidth: 600, minHeight: 480,
      resizeable: true,
      title: "Launcher",
      show: false,
      frame: false,
      enableLargerThanScreen: false,
      hasShadow: true,
      backgroundColor: "#fff"
    });



    global.launcherWebContents = launcherWin.webContents;
    ProcBridges();
    launcherWin.webContents.on('did-finish-load', () => PubSub.publish('launcher-did-finish-load'));

    launcherWin.on('close', () => {
      PubSub.publishSync('config.save');
    });

    launcherWin.loadURL(
      'file:///' + path.resolve(__dirname, '../ui/launcher.html')
    );

    launcherWin.show();

    PubSub.publish('everything-was-shown');
  });

};
