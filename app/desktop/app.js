/* @flow */
global.Promise = require('bluebird');

import path from 'path'
import Wins from './windows.js'
import Config from './config.js'
import Utils from './utils.js'
import Logger from './logger.js'
import Modpacks from './modpacks.js'
import PubSub from 'pubsub-js'

global.PubSub = PubSub;

Utils.Init();
Logger.Init(Utils);

Logger.Log('Launcher starting...', Logger.LogLevel.INFO);

Config.Init(Utils, Logger);

Modpacks.Init(Utils, Logger, Config);

process.on('uncaughtException', function (err) {
  Logger.Log(err.message, Logger.LogLevel.SEVERE);
  process.exit(-1);
});


Wins((app, launcherWin, consoleWin) => {
  Logger.Ready = true;
  Logger.Log('Launcher ready', Logger.LogLevel.INFO);
});
