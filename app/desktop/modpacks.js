import fs from 'fs'
import path from 'path'
import mkdirp from 'mkdirp'
import pcd from 'pcd'
import VerCompare from 'semver-compare'
import exists from 'exists-file'
import DecompressZip from 'decompress-zip'

var Promise = require('bluebird');

let writeFileAsync = Promise.promisify(fs.writeFile);
let request = Promise.promisifyAll(require('request'));

export default {

  Utils: void(0),
  Logger: void(0),

  ModpacksURL: 'http://127.0.0.1/modpacks/', //Slash at end is needed

  /*  Modpacks States  */
  /*
  *   Example state:
  *   {
  *     state: false,
  *     version: '1.0.4'
  *   }
  *
  */

  CheckInstalled: function CheckInstalledVersion(data) {

    PubSub.publish('modpacks.installed', this.Config.Get('InstalledModpacks'));

    let mpNeedsUpdate = {};
    let newVersions = {};

    this.Logger.Log('Data: ' + JSON.stringify(data), this.Logger.LogLevel.DEBUG);

    for(let mp of data.modpacks) {
      newVersions[mp.name] = mp.version;
    }

    let installed = this.Config.Get('InstalledModpacks');

    for(let k in installed) {
      mpNeedsUpdate[k] = VerCompare(installed[k].version, newVersions[k]) == -1;
    }

    PubSub.publish('modpacks.need-update', mpNeedsUpdate);
  },

  GetAssetIndex: function GetAssetIndex(version) {
    let ph = this.Utils.BasePath('assets' + path.sep + 'index' + path.sep + version + '.json')

    if(!exists.sync(path.dirname(ph)))
      mkdirp.sync(path.dirname(ph));

    if(!exists.sync(ph)) {
      console.log('Downloading assets index ' + version + '.json');
      return pcd.download('https://s3.amazonaws.com/Minecraft.Download/indexes/' + version + '.json', ph);
    }

    return Promise.resolve();
  },

  GetGameIndex: function GetAssetIndex(version) {
    let ph = this.Utils.BasePath('versions' + path.sep + version + path.sep + version + '.json')

    if(!exists.sync(path.dirname(ph)))
      mkdirp.sync(path.dirname(ph));

    if(!exists.sync(ph)) {
      console.log('Downloading game index ' + version + '.json');
      return pcd.download('https://s3.amazonaws.com/Minecraft.Download/versions/' + version + '/' + version + '.json', ph);
    }

    return Promise.resolve();
  },

  GetForgeIndex: function GetAssetIndex(mcVersion, forgeVersion) {
    let ph = this.Utils.BasePath('versions' + path.sep + mcVersion + path.sep + 'forge-' + forgeVersion + '.json')

    if(!exists.sync(path.dirname(ph)))
      mkdirp.sync(path.dirname(ph));

    if(!exists.sync(ph)) {
      console.log('Downloading forge index for ' + forgeVersion + '');

      return request
              .getAsync('https://raw.githubusercontent.com/LexManos/MinecraftForge/master/jsons/' + mcVersion + '-rel.json')
              .then(
                result => {
                  let toSave = {};
                  let toTearApart = JSON.parse(result.body);

                  toSave.minecraftArguments = toTearApart.versionInfo.minecraftArguments;
                  toSave.mainClass = toTearApart.versionInfo.mainClass;
                  toSave.libraries = toTearApart.versionInfo.libraries;

                  toSave.libraries.map(e => {
                    e.name = (e.name === '@artifact@' ? ('net.minecraftforge:forge:' + forgeVersion + ':universal') : e.name);
                    return e;
                  });

                  return writeFileAsync(ph, JSON.stringify(toSave, null, '\t'));
                }
              );
    }

    return Promise.resolve();
  },

  DownloadLibs: function DownloadLibraries(libs) {

    libs.map(lib => {
      let [libPackage, libName, libVersion, libClassifier] = lib.name.split(':');

      return {
        savePath: '',
        url: ''
      };
    });



    return Promise.resolve();
  },

  DownloadAssets: function DownloadAssets() {


    return Promise.resolve();
  },

  DownloadModpack: function DownloadModpack(modpackID) {

    let ph = this.Utils.BasePath('tmp' + path.sep + modpackID + '.zip');
    let uh = this.ModpacksURL + modpackID + '.zip';

    if(!exists.sync(path.dirname(ph)))
      mkdirp.sync(path.dirname(ph));
    
    var dwnl = pcd.single(uh, ph);

    dwnl.on('progress', data => {
      PubSub.publish('downloader', ['install:progress', (data[0] * 100) / data[1]]);
    });

    return new Promise(res => {
      dwnl.on('done', () => {
        this.Logger.Log('Modpack file downloaded', this.Logger.LogLevel.INFO);
        res();
      });
    
      dwnl.start();
    });
  },

  UnpackModpack: function UnpackModpack(modpackID) {

    let ph2 = this.Utils.BasePath('instances' + path.sep + modpackID + path.sep + 'gamefiles');

    if(!exists.sync(ph2))
      mkdirp.sync(ph2);

    return new Promise((res, rej) => {
      let ph = this.Utils.BasePath('tmp' + path.sep + modpackID + '.zip');
      let uzip = new DecompressZip(ph);

      uzip.on('error', rej);
      uzip.on('extract', res);
      uzip.on('progress', (fIndex, fCount) => PubSub.publish('download', ['install:unzip:progress', {done: fIndex + 1, count: fCount}]));

      this.Logger.Log('Unpacking...', this.Logger.LogLevel.INFO);

      PubSub.publish('download', ['install:unzip:start']);

      uzip.extract({
        path: ph2
      });

    });

  },

  Install: function InstallModpack(id, name, version, mcVersion, forgeVersion) {

    this.Logger.Log('Installing modpack \'' + name + '\'', this.Logger.LogLevel.INFO);

    PubSub.publish('downloader', ['install:fade-in']);

   this.DownloadModpack(id)
      .then(() => this.Logger.Log('Download done', this.Logger.LogLevel.INFO))
      .then(this.UnpackModpack(id))
      .then(Promise.all([
        this.GetGameIndex(mcVersion),
        this.GetForgeIndex(mcVersion, forgeVersion),
        this.GetAssetIndex(mcVersion)
      ]))
      .then(Promise.delay(1000))
      .then(() => {
        let gindex = require(this.Utils.BasePath('versions' + path.sep + mcVersion + path.sep + mcVersion + '.json'));
        let findex = require(this.Utils.BasePath('versions' + path.sep + mcVersion + path.sep + 'forge-' + forgeVersion + '.json'));
        return this.DownloadLibs([].concat(gindex.libraries, findex.libraries));
      })
      .then(() => {
        this.Logger.Log("Got all the indexes", this.Logger.LogLevel.DEBUG);
        PubSub.publish('downloader', ['install:fade-out']);
      }); //TODO: Continue worknig on this

    /*  Installation done, saving to config  */
    /*let conf = this.Config.Get('InstalledModpacks');
    conf[id] = {version: version, mcVersion: mcVersion};
    this.Config.Set('InstalledModpacks', conf);*/
  },

  Update: function UpdateModpack(name) {
    console.log('Updating modpack ' + name);
  },

  Launch: function LaunchModpack(name) {
    console.log('Launching modpack ' + name);
  },

  Init: function Init(Utils, Logger, Config) {

    this.Utils = Utils;
    this.Logger = Logger;
    this.Config = Config;

    PubSub.subscribe('modpacks.install', (e, data) => this.Install(data.id, data.name, data.version, data.mcVersion, data.forgeVersion));
    PubSub.subscribe('modpacks.update', (e, data) => this.Update(data.name));
    PubSub.subscribe('modpacks.launch', (e, data) => this.Launch(data.name));
    PubSub.subscribe('modpacks.got-data', (e, data) => this.CheckInstalled(data));

  }
}
