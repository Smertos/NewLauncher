import _ from 'underscore'

var SliderPanel = class SliderPanel extends React.Component {
  render () {
    return (
      <div className="slider-panel" id={"sp-" + this.props.iNum} >
        {this.props.yorSlideSlave}
      </div>
    );
  }
}

export default class Slider extends React.Component {
  constructor (props) {
    super(props);
    if(!props.slidesNum) console.error('this.props.slidesNum is not set');
    this.state = {
      slide: 0
    };
  }

  componentDidMount () {
    PubSub.subscribe('slider:new-tab', (msg, data) => {
      this.setState({
        slide: data
      });

      $('.slider').css('left', (-$('.head-container').width() * data) + 'px')
    });

    window.addEventListener('resize', e => $('.slider').css('left', (-$('.head-container').width() * this.state.slide) + 'px'));

    PubSub.publish('slider.onmount');
  }

  shouldComponentUpdate(newProps, newState) {
    return newState.slide == this.state.slide;
  }

  render () {
    var i = 0;

    $('.slider').css('left', (-$('.head-container').width() * this.state.slide) + 'px')

    return (
      <div className="slider">
        {
          _.range(this.props.slidesNum).map(x => <SliderPanel iNum={x} yorSlideSlave={this.props.slides[x]} />)
        }
      </div>
    );
  }
}
