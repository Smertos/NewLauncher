export default class InstallPane extends React.Component {

  state = {
    currState: 'install:download',
    extract: {
      done: -1,
      count: 0
    }
  }

  componentDidMount() {
    PubSub.subscribe('install:fade-in', this.fadeIn);
    PubSub.subscribe('install:fade-out', this.fadeOut);

    PubSub.subscribe('install:progress', this.onDownloadProgress);

    PubSub.subscribe('install:unzip:start', (e) => {

      console.log('Changing state to: unzip:start');

      setState({
        currState: 'install:extract',
        extract: {
          done: -1,
          count: 0
        }
      });
    });

    PubSub.subscribe('install:unzip:progress', (e, data) => {
      [done, count] = data;

      console.log('Extracted: ' + done + '/' + count + ' files');

      this.state.extract.done = done;
      this.state.extract.count = count;

      setState({
        extract: {done, count}
      });
    });

    this.state.el = ReactDOM.findDOMNode(this);
  }

  fadeIn = () => {
    this.setState({
      currState: 'install:download'
    });

    this.state.el.style.opacity = 1;
    this.state.el.style['z-index'] = 3;
  }

  fadeOut = () => {
      this.state.el.style.opacity = 0;
      this.state.el.style['z-index'] = -1;
  }

  onDownloadProgress = (e, data) => {
    if(!this.state.progressBar)
      this.state.progressBar = document.querySelector('.progressBar');

    console.log('Downloaded: ' + data + '%');

    this.state.progressBar.attributes['value'].value = data;
  }

  render() {

    return (<div id="download" className="fading-pane">
      <div className="paper-pane">
        {
          (() => {
            switch (this.state.currState) {
              case 'install:download':
                return [
                  <p><span className="top-title">Установка</span></p>,
                  <p><span className="sub-top-title">Загружаем модпак</span></p>,
                  <progress className="progressBar" value="0" max="100">
                    Идет загрузка...
                  </progress>
                ];

              case 'install:extract':
              return [
                <p><span className="top-title">Установка</span></p>,
                <p><span className="sub-top-title">{'Распаковано ' + this.state.extract.done + ' из ' + this.state.extract.count + ' файлов'}</span></p>,
                <progress className="progressBar2" value={this.extract.done} max={this.extract.count}>
                  Идет загрузка...
                </progress>
              ];

              case 'install:indices':
              return [
                <p><span className="top-title">Установка</span></p>,
                <p><span className="sub-top-title">Загружаем индексные файлы</span></p>
              ];

              default:
                return [];
            }
          })()
        }
      </div>
    </div>);
  }

}
