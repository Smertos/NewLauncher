global.Promise = require('bluebird');

import __procbridge from './javascripts/launcher-process-bridge.js'

import Titlebar from './javascripts/launcher-titlebar.js'
import Sidebar from './javascripts/launcher-sidebar.js'
import TitleScreen from './javascripts/launcher-titlescreen.js'
import Slider from './javascripts/launcher-slider.js'
import NewsTab from './javascripts/launcher-tab-news.js'
import ModpacksTab from './javascripts/launcher-tab-modpacks.js'
import SettingsTab from './javascripts/launcher-tab-settings.js'

import AuthPane from './javascripts/launcher-auth-pane.js'
import InstallPane from './javascripts/launcher-install-pane.js'

let slidespeed = 500;
let title = "Launcher";
let urls = {
  news: 'http://127.0.0.1/data.news.json',
  modpacks: 'http://127.0.0.1/data.modpacks.json'
};

let tabs = [<NewsTab url={urls.news}/>, <ModpacksTab url={urls.modpacks}/>, <SettingsTab/>];

//Window constants
window.onload = (function () {
  $('.slider').css('width', (tabs.length * 100) + '%');
});

//Render after constants set
ReactDOM.render(<div className="head-container league-gothic">
                  <Titlebar title={title} />
                  <Sidebar tabs={tabs.length} />
                  <TitleScreen />
                  <Slider slidesNum={tabs.length} slides={tabs} slidespeed={slidespeed} />
                  <InstallPane />
                  <AuthPane />
                </div>, document.body);
