export default class AuthPane extends React.Component {

  state = {}

  componentDidMount() {
    PubSub.subscribe('auth:fade-in', this.fadeIn);
    PubSub.subscribe('auth:fade-out', this.fadeOut);

    this.state.el = ReactDOM.findDOMNode(this);
  }

  fadeIn = () => {
    this.state.el.style.opacity = 1;
    this.state.el.style['z-index'] = 3;
  }

  fadeOut = () => {
      this.state.el.style.opacity = 0;
      this.state.el.style['z-index'] = -1;
  }

  render() {
    return (<div id="auth" className="fading-pane">

    </div>);
  }

}
