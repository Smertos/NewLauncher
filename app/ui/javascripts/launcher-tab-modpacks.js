import marked from 'marked'

class ModpacksItem extends React.Component {

  state = {
    opened: false,
    containerClass: "modpacks-item no-select",
    installed: false,
    needsUpdate: false
  }

  componentDidMount() {

    this.state.el = $(ReactDOM.findDOMNode(this));

    PubSub.subscribe('installed.' + this.props.name, (e, data) => {
      this.setState({
        installed: true
      });
    });

    PubSub.subscribe('need-update.' + this.props.name, (e, data) => {
      this.setState({
        needsUpdate: data
      });
    });

  }

  onClick = (e) => {

    if(e.target.tagName !== 'SPAN' || !this.state.opened) {

      if(this.state.opened) {

        this.state.el.css('max-height', '80px');
        $(this.state.el.find('p').find('img').get(0))
          .css('border-bottom-left-radius', '0');

        this.setState({
          containerClass: "modpacks-item no-select"
        });


      } else {

        this.state.el.css('max-height', '8000px');
        $(this.state.el.find('p').find('img').get(0))
          .css('border-bottom-left-radius', '20px');

        this.setState({
          containerClass: "modpacks-item"
        });

      }

      this.setState({
        opened: !this.state.opened
      });

    }

  }

  onLaunch = () => {
    console.log('Starting \'' + this.props.name + '\' modpack');

    PubSub.publish(this.state.installed ? (this.state.needsUpdate ? 'update' : 'launch') : 'install', {
      id: this.props.id,
      name: this.props.name,
      version: this.props.version,
      mcVersion: this.props.mcVersion,
      forgeVersion: this.props.forgeVersion
    });

  }

  render() {

    return (
      <div className={this.state.containerClass} onClick={this.onClick}>


        <p className="modpacks-item-top-p">

          <span>
            {this.props.name}
          </span>

          <img className="modpacks-item-icon" src={this.props.iconURL} />

        </p>

        <p>

          <span className="modpack-desc">
            <b>Description:</b> <div dangerouslySetInnerHTML={{ __html: marked(this.props.description) }}/>
          </span>

        </p>

        <p>

          <span className="modpack-ver">
            <b>Version:</b> <div dangerouslySetInnerHTML={{ __html: marked(this.props.version) }}/>
          </span>

        </p>

        <p>

          <span className="modpack-mcver">
            <b>Minecraft Version:</b> <div dangerouslySetInnerHTML={{ __html: marked(this.props.mcVersion) }}/>
          </span>

        </p>

        <p>

          <span className="modpack-mods">
            <b>Mods:</b>
          </span>

          <ul className="mods-list">
            {
              this.props.mods.map(
                x => <li className="mod-item">
                      <span>
                        <div dangerouslySetInnerHTML={{ __html: marked(x) }}/>
                      </span>
                     </li>
              )
            }
          </ul>

        </p>

        <div className={ this.state.installed ? (this.state.needsUpdate ? "ibtn ubtn" : "ibtn lbtn") : "ibtn" } onClick={this.onLaunch}>
          <span>{ this.state.installed ? (this.state.needsUpdate ? "Update" : "Launch") : "Install" }</span>
        </div>


      </div>
    );
  }
}


export default class ModpacksTab extends React.Component {
  constructor (props) {
    super(props);

    $.ajax(this.props.url, {
      method: 'GET'/*'POST'*/,
      complete: (jqXHR, textStatus) => {
        let mObj = JSON.parse(jqXHR.responseText);
        PubSub.publish('modpacks.data', mObj);
        this.setState(mObj);
      }
    });

    this.state = {
      modpacks: []
    };
  }

  render() {
    return (
      <div className="slider-tab" id="tab-modpacks">
        <div className="modpacks-list">
          {
            this
            .state
            .modpacks
            .map(
              x => <
                ModpacksItem
                  id={x.id}
                  name={x.name}
                  version={x.version}
                  mcVersion={x.mcVersion}
                  description={x.description}
                  iconURL={x.iconURL}
                  mods={x.mods}
                  forgeVersion={x.forgeVersion}
              />
            )
          }
        </div>
      </div>
    );
  }
}
