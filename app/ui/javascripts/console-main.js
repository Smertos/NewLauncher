global.Promise = require('bluebird');

import {ipcRenderer} from 'electron'

import LogPanel from './javascripts/console-log-panel.js'

let limit = 500;

ipcRenderer.on('log', (e, data) => {
  console.log('Got log line: ' + data.message);
  PubSub.publish('log', data);
});

ipcRenderer.on('logstack', (e, data) =>{
  console.log('Got late log: ' + data);
  data.forEach( el =>  PubSub.publish('log', el) );
});

ReactDOM.render(<LogPanel limit={limit}/>, document.querySelectorAll('.log-pan')[0]);
