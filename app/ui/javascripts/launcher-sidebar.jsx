import _ from 'underscore'

class SidebarButtonListElem extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      icon: (function(n) {
        switch (n) {
          case 0: return "receipt"
          case 1: return "reorder"
          case 2: return "settings"
          default: return "label outline"
        }
      })(this.props.n),
      label: (function(n) {
        switch (n) {
          case 0: return "Новости"
          case 1: return "Модпаки"
          case 2: return "Настройки"
          default: return "???"
        }
      })(this.props.n)
    };

    this.onSwitch = this.onSwitch.bind(this);
  }

  onSwitch () {
    _
    .without(_.range(this.props.tabs), Number(this.props.n))
    .map(num => $('#top-tab-' + num))
    .forEach(x => x.removeClass('tab-selected'));

    PubSub.publish('slider:new-tab', Number(this.props.n));

    $('#top-tab-' + this.props.n).addClass('tab-selected');
  }

  render() {
    return (
      <li className={!this.props.n
                        ? "sidebar-button top-tab tab-selected"
                        : "sidebar-button top-tab"}
                id={"top-tab-" + this.props.n}  onClick={this.onSwitch}
      >
        <div className="magic-center">
          <i className="material-icons md-light magic-center">
            {this.state.icon}
          </i>
          <span className="magic-center">
            {this.state.label}
          </span>
        </div>
      </li>
    );
  }
}

export default class Sidebar extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      current: 0
    };
  }

  componentDidMount () {
    PubSub.subscribe('slider:new-tab', (msg, data) => {
      this.setState({
        current: data
      });
    });

    PubSub.subscribe('sidebar:show', (msg, data) => {
      $(ReactDOM.findDOMNode(this)).css('height', '64px').css('transition-duration', '0.2s');
    });
  }

  render() {
    return (
      <div className="sidebar-top">
        <ul className="sidebar-button-list">
          {
            _
              .range(this.props.tabs)
              .map(
                x => <
                  SidebarButtonListElem
                    n={x}
                    tabs={this.props.tabs}
                />
              )
          }
        </ul>
      </div>
    );
  }
}
