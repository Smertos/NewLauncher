export default React.createClass({
  componentDidMount: function () {
    /*$('#tsMain').animate({'opacity': 1}, 900, () => {
      $('#tsSecond').animate({'opacity': 1}, 900, () => {
        setTimeout(x => $('.title-screen').animate({'opacity': 0, 'z-index': -1}, 600), 1200);
      });
    });*/
    setTimeout(x => $('#tsMain').css('opacity', 1), 20);
    setTimeout(x => $('#tsSecond').css('opacity', 1), 900);
    setTimeout(x => $('.title-screen').animate({'opacity': 0, 'z-index': -1}, 600), 2100);
    setTimeout(x => PubSub.publish('titlebar:show'), 2150);
    setTimeout(x => PubSub.publish('sidebar:show'), 2250);


  },

  render: function() {
    return (
      <div className="title-screen">
        <span id="tsMain">EGBox</span>
        <span id="tsSecond">
          Мы любим вас :3
        </span>
      </div>
    );
  }
});
