class RAMSelector extends React.Component {

  state = {
    value: 512
  }

  onChange = (e) => {
    this.setState({
      value: e.target.value
    });
  }

  render() {

    return (
      <div id="ram-selector">
        <p>
          <span>Выделить памяти игре:</span>
          <span id="ram-size">{this.state.value + ' MB'}</span>
        </p>
        <input className="settings-slider" type="range" min="512" max="4096" step="256" value={this.state.value} onChange={this.onChange} />
      </div>
    );

  }

}

class PermGenSelector extends React.Component {

  state = {
    value: 128,
    java8: false
  }

  onChange = (e) => {
    this.setState({
      value: e.target.value
    });
  }

  render() {

    return (
      <div id="ram-selector">
        <p>
          <span>Выделить {this.state.java8 ? "MetaSpace" : "PermGen"} памяти игре:</span>
          <span id="ram-size">{this.state.value + ' MB'}</span>
        </p>
        <input className="settings-slider" type="range" min="128" max="1024" step="8" value={this.state.value} onChange={this.onChange} />
      </div>
    );

  }

}

class NamedCheckbox extends React.Component {

  state = {
    checked: false
  }

  componentDidMount() {
    var jEl = $(ReactDOM.findDOMNode(this));
    jEl.on('click', (e) => {
      console.log('Click!');
      this.state.checked = !this.state.checked;
      jEl.find('input').get(0).checked = this.state.checked;
    });
  }

  render() {
    return (
      <div>
        <input type="checkbox" />
        <label>{this.props.name}</label>
      </div>
    );
  }
}

export default class SettingsTab extends React.Component {

  render () {

    return (
      <div className="slider-tab" id="tab-settings">
        <div className="settings-list">

          <div className="settings-page">
            <p>
              <span className="settings-page-title">Найстройки лаунчера</span>
            </p>
            <p>
              <NamedCheckbox name="Скрывать консоль при запуске игры" />
            </p>
          </div>

        </div>


        <div className="settings-list">

          <div className="settings-page">
            <p>
              <span className="settings-page-title">Настройки игры</span>
            </p>
            <RAMSelector />
            <div className="splitter"></div>
            <PermGenSelector />
          </div>

        </div>
      </div>
    );

  }

}
