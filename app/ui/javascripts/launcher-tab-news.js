import marked from 'marked'

class NewsItem extends React.Component {
  render () {
    return (
      <div className="news-list-item">


        <p className="nli-top-p">

          <span className="nli-title">
            {this.props.title}
          </span>

          <span className="nli-date-shade">
            {this.props.date}
          </span>

        </p>


        <p>

          <div className="nli-text">
            <div dangerouslySetInnerHTML={{ __html: marked(this.props.text) }}/>
          </div>

        </p>


      </div>
    );
  }
}

export default class NewsTab extends React.Component {
  constructor(props) {
    super(props);

    /*$.ajax(this.props.url, {
      method: POST
    });*/

    this.state = {
      newsList: [


        {
          date: "03.03.2016",
          title: "Годная новость",
          text: "Сегодня шел такой по *улице*, расказываю другане как быстро его засосет черная дыра размером с копейку. После слов '**Так быстро засосет, что...**' я пиздякнулся на льду и мы оба выпали ржать. Конец."
        },

        {
          date: "03.03.2016",
          title: "Test",
          text: '![](http://cs633916.vk.me/v633916017/157a2/wBF916BnHhA.jpg)'
        },

        {
          date: "03.03.2016",
          title: "Test2",
          text: '![](https://media.giphy.com/media/10RhccNxPSaglW/giphy.gif)'
        },

        {
          date: "03.03.2016",
          title: "Test2",
          text: "SPAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE!!!!!!!!!!!!!111111111111"
        },

        {
          date: "03.03.2016",
          title: "Test2",
          text: "SPAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE!!!!!!!!!!!!!111111111111"
        },

        {
          date: "03.03.2016",
          title: "Test2",
          text: "SPAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE!!!!!!!!!!!!!111111111111"
        }


      ]
    };
  }

  render () {
    return (
      <div className="slider-tab" id="tab-news" align="center">
        <div className="news-list">
          {
            this
              .state
              .newsList
              .map(
                x => <
                  NewsItem
                    date={x.date}
                    title={x.title}
                    text={x.text}
                />
              )
          }
        </div>
      </div>
    );
  }
}
