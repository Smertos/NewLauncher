import {ipcRenderer} from 'electron'

export default (function() {

  /*  Listeners  */
  ipcRenderer.on('need-update', (e, data) => {
    for(let k in data)
      PubSub.publish('need-update.' + k, data[k]);
  });

  ipcRenderer.on('installed', (e, data) => {
    for(let k in data)
      PubSub.publish('installed.' + k);
  });

  ipcRenderer.on('downloader', (e, data) => {
    console.log('fired ' + data[0]);
    PubSub.publish(data[0], data[1]);
  });

  /*  Fires  */
  PubSub.subscribe('update', (e, data) => ipcRenderer.send('update', data));
  PubSub.subscribe('launch', (e, data) => ipcRenderer.send('launch', data));
  PubSub.subscribe('install', (e, data) => ipcRenderer.send('install', data));
  PubSub.subscribe('modpacks.data', (e, data) => ipcRenderer.send('modpacks-data', data));

})();
