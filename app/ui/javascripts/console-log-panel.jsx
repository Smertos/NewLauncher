import _ from 'underscore'

class LimitedArray extends Array {

  limit = Infinity

  constructor(__limit) {
    super(0);

    if(typeof __limit === 'number')
      this.limit = __limit;
  }

  push(elem) {
    if(this.length == this.limit)
      this.shift();

    Array.prototype.push.call(this, elem);
  }
}

export default class LogPanel extends React.Component {

  state = {}

  constructor(props) {
    super(props);

    this.state.msgs = new LimitedArray(this.props.limit);

    PubSub.subscribe('log', (e, data) =>{
      let k = this.state.msgs;
      k.push(data);
      this.setState({
        msgs: k
      });
    });
  }

  render() {
    return <div className="wrapper">{_.map(this.state.msgs, (e, k) => <div className={"log-line-" + e.type} key={k}>{'[' + e.type.toUpperCase() + '] ' + e.message}</div>)}</div>;
  }
}
