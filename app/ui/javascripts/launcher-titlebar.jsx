export default class TitleBar extends React.Component {
  constructor(props) {
    super(props);

    PubSub.subscribe('titlebar:show', (msg, data) => {
      $(ReactDOM.findDOMNode(this)).css('height', '25px').css('padding-top', '3px');
    });
  }

  render() {
    return (
      <div className="title-top no-select" id="titlebar">
        <div className="title-name">{this.props.title}</div>
      </div>
    );
  }
}
