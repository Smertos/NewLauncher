var gulp = require('gulp');
var babel = require('gulp-babel');
var liver = require('gulp-livereload');
var uglify = require('gulp-uglify');
var path = require('path');
var spawn = require('child_process').spawn;
var packager = require('electron-packager');
var eInst = require('electron-winstaller');

var packageJSON = require('./package.json');

/*  Variables  */
var compileFiles = [
  "app/desktop/*.js",
  "app/ui/javascripts/*.js",
  "app/ui/javascripts/*.jsx"
];

var copyFiles = [
  "app/ui/*.html",
  "app/ui/stylesheets/*.css",
  "app/ui/stylesheets/*.otf",
  "app/ui/stylesheets/*.ttf",
  "app/ui/stylesheets/*.woff",
  "app/ui/stylesheets/*.woff2",
  "app/ui/components/jquery/dist/jquery.min.js",
  "app/ui/components/react/react.min.js",
  "app/ui/components/react/react-dom.min.js"
];

var uglyCopy = [
  "app/ui/components/PubSubJS/src/pubsub.js",
  "app/ui/components/react/react.js",
  "app/ui/components/react/react-dom.js"
];

var runArgs = [
  '.',
  '--disable-http-cache'
];

/*  Tasks  */

gulp.task('start', () => {
  var launcher = spawn('./node_modules/electron-prebuilt/dist/electron', runArgs);

  launcher.stdout.on('data', (data) => {
    console.log(''+data);
  });

  launcher.stderr.on('data', (data) => {
    console.log(''+data);
  });

  launcher.on('close', (code) => {
    console.log(`launcher process exited with code ${code}`);
  });
});

gulp.task('compile', () => {
  compileFiles.forEach(
    x => gulp.src(x)
            .pipe(babel())
            //.pipe(uglify())
            .pipe(
              gulp.dest(
                path.dirname(x.replace('app', 'dist'))
              )
            )
            .pipe(liver())
  );
});

gulp.task('copy', () => {
  copyFiles.forEach(
    x => gulp.src(x)
            .pipe(
              gulp.dest(
                path.dirname(x.replace('app', 'dist'))
              )
            )
            .pipe(liver())
  );

  uglyCopy.forEach(
    x => gulp.src(x)
            .pipe(uglify())
            .pipe(
              gulp.dest(
                path.dirname(x.replace('app', 'dist'))
              )
            )
            .pipe(liver())
  );
});

gulp.task('watch', ['compile', 'copy'], () => {

  liver.listen(4533);

  gulp.watch(compileFiles, (e) => {
    if(e.type === 'changed') {
      console.log('File ' + e.path + ' was modified. Compiling...');
      return gulp.src(e.path)
              .pipe(babel())
              .pipe(
                gulp.dest(
                  path.dirname(e.path.replace('app', 'dist'))

                )
              ).pipe(liver());
    }
  });

  gulp.watch(copyFiles, (e) => {
    if(e.type === 'changed') {
      console.log('File ' + e.path + ' was modified. Copying...');
      return gulp.src(e.path)
              .pipe(
                gulp.dest(
                  path.dirname(e.path.replace('app', 'dist'))
                )
              ).pipe(liver());
    }
  });

});

gulp.task('pack', () => {
  //spawn();
  switch (process.argv[3]) {
    case '-linux': {
      console.log('Packing for Linux x32 & x64');
      packager({
        dir: './dist/',
        platform: 'linux',
        arch: 'all',
        'app-version': packageJSON.version,
        asar: true,
        name: 'Launcher',
        out: './pack/linux/',
        version: '0.36.8',
        overwrite: true
      }, () => console.log('Packing for Linux done.'));
      break;
    }



    case '-mac': {
      console.log('Not implemented yet');
      break;
    }



    case '-win': default: { //Pack for win
      console.log('Packing for Win x32 & x64');
      packager({
        dir: './dist/',
        platform: 'win32',
        arch: 'all',
        'app-version': packageJSON.version,
        asar: true,
        name: 'Launcher',
        out: './pack/win/',
        version: '0.36.8',
        overwrite: true
      }, () => console.log('Packing for Windows done.'));
    }
  };
});

gulp.task('make-installer', () => {
  //spawn();
  switch (process.argv[3]) {
    case '-linux': {
      console.log('Not implemented yet');
      break;
    }



    case '-mac': {
      console.log('Not implemented yet');
      break;
    }



    case '-win': default: { //Pack for win
      console.log('Making installer for Win x32 & x64');
      eInst.createWindowsInstaller(
        {
          appDirectory: path.resolve('./pack/Launcher-win32-x64/'),
          outputDirectory: path.resolve('./build/x64'),
          authors: 'Rusty Bullet Hole Inc.',
          exe: 'Launcher.exe',
          authors: 'Smertos'
        }
      )
      .then(
        x => eInst.createWindowsInstaller(
          {
            appDirectory: path.resolve('./pack/Launcher-win32-ia32/'),
            outputDirectory: path.resolve('./build/x32'),
            authors: 'Rusty Bullet Hole Inc.',
            exe: 'Launcher.exe',
            authors: 'Smertos'
          }
        )
      )
      .then(() => console.log('Making installer for Windows done.'));
    }
  };
});

gulp.task('build', ['compile', 'copy', 'pack']);

gulp.task('default', ['build']);
